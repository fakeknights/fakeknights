import * as React from 'react';
import './App.css';

import logo from './logo.jpg';

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <img src={logo} className="App-logo" alt="logo" />
        <header className="App-header">
          <h1 className="App-title">Welcome to Fake Knights!</h1>
        </header>
      </div>
    );
  }
}

export default App;
